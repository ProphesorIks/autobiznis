-- Your SQL goes here
CREATE TABLE jobs (
  uuid VARCHAR NOT NULL PRIMARY KEY,
  status VARCHAR NOT NULL,
  created_timestamp BIGINT NOT NULL,
  expires BIGINT NOT NULL
)
