#[macro_use]
extern crate dotenv_codegen;

pub mod database;
pub mod job;
pub mod models;
pub mod routes;
pub mod scheduler;
pub mod schema;
pub mod tasks;
pub mod utils;

use axum::{
    extract::FromRef,
    http::{Response, StatusCode},
    response::IntoResponse,
    Router,
};
use handlebars::Handlebars;
use routes::api_router;
use tokio::task::JoinSet;
use tower_http::cors::{Any, CorsLayer};

#[derive(Clone)]
pub struct AppState {}

impl AppState {
    fn new() -> Self {
        Self {}
    }
}

#[tokio::main]
async fn main() {
    let _app_environment = dotenv!("APP_ENVIRONMENT");
    let app_host = dotenv!("APP_HOST");
    let app_port = dotenv!("APP_PORT");
    let cors = CorsLayer::new().allow_origin(Any);
    let state = AppState::new();

    let mut handles = JoinSet::new();
    handles.spawn(async {
        tasks::task_cleanup().await;
    });

    handles.spawn(async {
        tasks::task_generate_invoice().await;
    });

    let app = Router::<AppState>::new()
        .nest("/v1", api_router())
        .with_state(state)
        .layer(cors);

    let address = format!("{}:{}", app_host, app_port);
    let listener = tokio::net::TcpListener::bind(&address).await.unwrap();

    println!("Serving on: http://{}", address);
    let serve = axum::serve(listener, app);
    handles.spawn(async {
        serve.await.unwrap();
    });

    while let Some(_) = handles.join_next().await {}
    println!("exiting...");
}
