use diesel::{
    r2d2::{ConnectionManager, Pool},
    SqliteConnection,
};
use lazy_static::lazy_static;
lazy_static! {
    pub static ref DATABASE: Pool<ConnectionManager<SqliteConnection>> = {
        let database_url = dotenv!("DATABASE_URL");
        let manager = ConnectionManager::<diesel::SqliteConnection>::new(database_url);
        let pool = diesel::r2d2::Pool::builder()
            .build(manager)
            .expect("Failed to create pool.");

        pool
    };
}
