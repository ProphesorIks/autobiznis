use chrono::serde::ts_milliseconds;
use chrono::{prelude::*, Duration};
use serde::{Deserialize, Serialize};
use std::{path::PathBuf, str::FromStr};
use uuid::Uuid;

use crate::{models::JobModel, utils::enum_type_from_str};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum JobStatus {
    Created,
    Started,
    Finished,
    Canceled,
    Failed,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum JobType {
    InvoicePdfGenerate,
}

#[derive(Serialize, Deserialize)]
pub struct JobHandle {
    uuid: Uuid,
}

#[derive(Serialize)]
pub struct Job {
    uuid: Uuid,
    pub job_status: JobStatus,
    #[serde(rename = "type")]
    type_: JobType,
    #[serde(with = "ts_milliseconds")]
    created_at: chrono::DateTime<Utc>,
    #[serde(with = "ts_milliseconds")]
    pub expires: chrono::DateTime<Utc>,
    pub metadata: Option<serde_json::Value>,
}

impl Default for Job {
    fn default() -> Self {
        let created_at = Utc::now();
        let expires = created_at + Duration::seconds(600);
        Self {
            uuid: Uuid::new_v4(),
            job_status: JobStatus::Created,
            type_: JobType::InvoicePdfGenerate,
            created_at,
            expires,
            metadata: None,
        }
    }
}

const ARTIFACTS_DIR: &str = "artifacts";

impl Job {
    pub fn start(metadata: Option<serde_json::Value>) -> Result<Self, ()> {
        let mut j = Job::default();
        j.metadata = metadata;
        let j = j;
        let uuid = j.uuid.to_string();

        std::fs::create_dir(format!("{}/{}", ARTIFACTS_DIR, uuid)).map_err(|_| ())?;
        std::fs::create_dir(format!("{}/{}/in", ARTIFACTS_DIR, uuid)).map_err(|_| ())?;
        std::fs::create_dir(format!("{}/{}/out", ARTIFACTS_DIR, uuid)).map_err(|_| ())?;

        Ok(j)
    }

    pub fn cleanup(&self) -> Result<(), ()> {
        std::fs::remove_dir_all(format!("{}/{}", ARTIFACTS_DIR, self.uuid)).map_err(|_| ())
    }

    pub fn get_uuid(&self) -> Uuid {
        self.uuid
    }

    pub fn get_path_in(&self) -> PathBuf {
        PathBuf::try_from(&format!("{}/{}/in", ARTIFACTS_DIR, self.get_uuid())).unwrap()
    }

    pub fn get_path_out(&self) -> PathBuf {
        PathBuf::try_from(&format!("{}/{}/out", ARTIFACTS_DIR, self.get_uuid())).unwrap()
    }
}

impl TryFrom<&JobModel> for Job {
    type Error = ();
    fn try_from(value: &JobModel) -> Result<Self, Self::Error> {
        let res = Self {
            uuid: Uuid::from_str(&value.uuid).map_err(|_| {
                println!("a");
            })?,
            job_status: enum_type_from_str::<JobStatus>(value.status.as_ref())
                .map_err(|_| println!("b"))?,
            created_at: chrono::DateTime::<Utc>::from_timestamp_millis(value.created_timestamp)
                .ok_or(())?,
            expires: chrono::DateTime::<Utc>::from_timestamp_millis(value.expires).ok_or(())?,
            type_: enum_type_from_str(value.type_.as_ref()).map_err(|_| println!("c"))?,
            metadata: match value.metadata.as_ref() {
                Some(d) => serde_json::from_str(d).expect("metadata is not a valid JSON"),
                None => None,
            },
        };

        Ok(res)
    }
}

impl From<&Job> for JobModel {
    fn from(value: &Job) -> Self {
        Self {
            uuid: value.uuid.to_string(),
            status: serde_variant::to_variant_name(&value.job_status)
                .unwrap()
                .to_string(),
            type_: serde_variant::to_variant_name(&value.type_)
                .unwrap()
                .to_string(),
            created_timestamp: value.created_at.timestamp_millis(),
            expires: value.expires.timestamp_millis(),
            metadata: value.metadata.as_ref().map(|v| {
                serde_json::to_string(&v).expect("metadata coudln't be serialized to JSON")
            }),
        }
    }
}

impl From<Job> for JobHandle {
    fn from(value: Job) -> Self {
        JobHandle {
            uuid: value.get_uuid(),
        }
    }
}

impl JobHandle {
    pub fn get_uuid(&self) -> Uuid {
        self.uuid
    }
}
