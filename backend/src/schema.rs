// @generated automatically by Diesel CLI.

diesel::table! {
    jobs (uuid) {
        uuid -> Text,
        status -> Text,
        created_timestamp -> BigInt,
        expires -> BigInt,
        #[sql_name = "type"]
        type_ -> Text,
        metadata -> Nullable<Text>,
    }
}
