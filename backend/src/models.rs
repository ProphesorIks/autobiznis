use diesel::prelude::*;
use serde::{Deserialize, Serialize};

use super::schema;

#[derive(Queryable, Insertable, Serialize, Deserialize, Identifiable, AsChangeset)]
#[diesel(table_name = schema::jobs)]
#[diesel(primary_key(uuid))]
pub struct JobModel {
    pub uuid: String,
    pub status: String,
    pub created_timestamp: i64,
    pub expires: i64,
    pub type_: String,
    pub metadata: Option<String>,
}
