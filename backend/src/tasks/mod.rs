use std::{fmt::format, io::Read, ops::Sub, path::Path, process::Command};

use crate::{
    database::DATABASE,
    job::{Job, JobStatus, JobType},
    models::JobModel,
    routes::invoice_generator::{template_from_invoice, CsvReadError, InvoiceMetadata, InvoiceRow, TplData, HBS},
    utils, AppState,
};

use bytes::Bytes;
use diesel::{BoolExpressionMethods, Connection, ExpressionMethods, QueryDsl, RunQueryDsl};
use serde_variant::to_variant_name;
use tokio::{fs, task::{JoinHandle, JoinSet}};
use anyhow::{anyhow, Context, Result};
use diesel::prelude::*;
use serde_json::json;
use crate::schema::jobs::dsl::*;

pub async fn task_cleanup() {
    use crate::schema::jobs::dsl::*;
    const JOBS_CLEANUP_LIMIT: i64 = 10;
    utils::set_interval(tokio::time::Duration::from_secs(30), || {
        tokio::spawn(async {
            let current_date_time = chrono::Utc::now();
            let current_timestamp = current_date_time.timestamp_millis();

            let job_rs: Result<Vec<JobModel>, _> =
                DATABASE
                    .get()
                    .unwrap()
                    .transaction(|conn| -> Result<Vec<JobModel>, _> {
                        jobs.filter(expires.lt(current_timestamp))
                            .limit(JOBS_CLEANUP_LIMIT)
                            .load(conn)
                    });

            match job_rs {
                Ok(v) => {
                    let ka: Vec<Result<String, ()>> = v
                        .into_iter()
                        .map(|m| {
                            let r = Job::try_from(&m).ok();
                            if let Some(rr) = r {
                                if let Ok(_) = rr.cleanup() {
                                    return Ok(rr.get_uuid().to_string());
                                }
                            } else {
                                println!("Couldn't convert job model to job");
                            }

                            return Err(());
                        })
                        .collect();

                    let uuids: Vec<String> = ka.into_iter().filter_map(|r| r.ok()).collect();
                    let s = DATABASE
                        .get()
                        .unwrap()
                        .transaction(|conn| -> Result<usize, _> {
                            diesel::delete(jobs.filter(uuid.eq_any(uuids))).execute(conn)
                        })
                        .ok();

                    if let Some(ar) = s {
                        println!("Deleted complete jobs: {}", ar);
                    } else {
                        print!("couldn't delete jobs!");
                    }
                }
                Err(_) => println!("Couldn't load jobs from db"),
            }
        });
    })
    .await;
}
pub async fn task_generate_invoice() {
    use crate::schema::jobs::dsl::*;
    println!("Starting invoice generation task");

    utils::set_interval(tokio::time::Duration::from_secs(3), || {
        tokio::spawn(async {
            let created_status = to_variant_name(&JobStatus::Created).expect("Failed to convert job status");
            let invoice_job_type = to_variant_name(&JobType::InvoicePdfGenerate).expect("Failed to convert job type");
            
            let pending_jobs: Vec<JobModel> = DATABASE.get()
                .unwrap()
                .transaction(|conn| {
                    jobs.filter(status.eq(created_status).and(type_.eq(invoice_job_type)))
                        .limit(1)
                        .load(conn)
                })
                .expect("Failed to load jobs from database");
            
                if let Some(job_model) = pending_jobs.into_iter().next() {
                    let job = Job::try_from(&job_model).expect("Failed to parse job model");
                    let job_id = job.get_uuid().to_string();
                    let update_job = diesel::update(jobs.find(job_id.clone()));


                    match process_invoice_task(job).await {
                        Ok(_) => {
                            let status_finished = to_variant_name(&JobStatus::Finished).expect("Failed to convert status");
                                    DATABASE.get().unwrap().transaction(|conn| {
                                        let query = update_job.clone().set(status.eq(status_finished));
                                        query.execute(conn)
                                    }).expect("Failed to update job status to Finished");
                            println!("completed invoice generate job task");
                        },
                        Err(e) => {
                            let status_failed = to_variant_name(&JobStatus::Failed).expect("Failed to convert status");
                            DATABASE.get().unwrap().transaction(|conn| {
                                let query = update_job.clone().set(
                                    (status.eq(status_failed),
                                    metadata.eq(Some(json!(format!("{:?}",e)).to_string())))
                                );
                                query.execute(conn)
                            }).expect("Failed to update job status to Failed");
                            println!("failed invoice generate job task");
                        }
                    }
                }
        });
    })
    .await;
}

async fn process_invoice_task(job: Job) -> Result<()> {
    use crate::schema::jobs::dsl::*;

    let job_id = job.get_uuid().to_string();
    let update_job = diesel::update(jobs.find(job_id.clone()));

    // Set job status to "Started"
    let status_started = to_variant_name(&JobStatus::Started).context("Failed to convert status")?;
    DATABASE.get().unwrap().transaction(|conn| {
        let query = update_job.clone().set(status.eq(status_started));
        query.execute(conn)
    }).context("Failed to update job status to Started")?;

    let mut input_path = job.get_path_in();
    input_path.push("in.csv");

    let input_file: bytes::Bytes = fs::read(input_path)
        .await
        .context("Input file not found")?
        .into();

    let csv_reader = csv::ReaderBuilder::new()
        .delimiter(b',')
        .from_reader(input_file.as_ref())
        .into_records();

    let mut invoice_rows = Vec::new();
    let invoice_meta: InvoiceMetadata = serde_json::from_value(
        job.metadata.clone().context("Missing metadata")?,
    )
    .context("Failed to deserialize metadata")?;

    let mut error_messages = Vec::new();

    for (idx, record) in csv_reader.enumerate() {
        let row = InvoiceRow::try_from(&record.context("Invalid CSV record")?).map_err(|e| match e {
            CsvReadError::Date => anyhow!("Netinkama data"),
            CsvReadError::EmptyField(f) => anyhow!("Trūksta būtino stulpelio: {}", f),
            CsvReadError::FieldType(f) => anyhow!("Nežinomas stulpelio tipas: {}", f),
        });
    
        match row {
            Ok(r) => invoice_rows.push(r),
            Err(e) => {
                let value = format!("Nepavyko perskaityti eilutės: #{: >4}. Klaida: {:?}", idx + 2, e);
                error_messages.push(value)
            }
        }
    
    }
    
    if !error_messages.is_empty() {
        return Err(anyhow!(error_messages.join("\n")));
    }

    invoice_rows.sort_by(|a, b| a.date.cmp(&b.date));
    let output_path = job.get_path_out();

    let mut handles = JoinSet::new();
    for (index, row) in invoice_rows.into_iter().enumerate() {
        let path = output_path.clone();
        let serial_number = invoice_meta.serial_start as i32 + index as i32;

        handles.spawn(async move {
            let template_name = to_variant_name(&row.template).context(format!("Nerastas šablono failas: {:?}", &row.template) )?;
            let invoice_data = template_from_invoice(row, serial_number);
            let filename = invoice_data.serial_num.clone();

            let mut html_path = path.clone();
            html_path.push(format!("{}.html", filename));

            let mut pdf_path = path.clone();
            pdf_path.push(format!("{}.pdf", filename));

            let mut html_writer = std::fs::File::create(&html_path)
                .context("Failed to create HTML file")?;
            HBS.render_to_write(template_name, &invoice_data, &mut html_writer)?;

            let pdf_generation = Command::new("wkhtmltopdf")
                .args(["--encoding", "utf-8"])
                .arg(html_path.to_str().unwrap())
                .arg(pdf_path.to_str().unwrap())
                .output()
                .context("Failed to execute wkhtmltopdf")?;

            if pdf_generation.status.success() {
                let _ = std::fs::remove_file(html_path);
            } else {
                anyhow::bail!("PDF generation failed");
            }

            Ok::<(), anyhow::Error>(())
        });
    }

    while let Some(handle) = handles.join_next().await {
       let _a = handle?;
    }

    let mut zip_path = output_path.clone();
    zip_path.push("out.zip");

    let zip_result = Command::new("zip")
        .arg("-j")
        .arg("-r")
        .arg(&zip_path)
        .arg(&output_path)
        .output()
        .context("Failed to execute zip command")?;

    if !zip_result.status.success() {
        let zip_error = String::from_utf8(zip_result.stderr).unwrap_or_else(|_| "Unknown error".to_string());
        anyhow::bail!("Zip command failed: {}", zip_error);
    }
    Ok(())
}
