use crate::AppState;

use self::{invoice_generator::route_invoice_generator, job::route_job};
use axum::{
    response::IntoResponse,
    routing::{get, MethodRouter},
    Router,
};

pub mod invoice_generator;
pub mod job;

async fn hello() -> impl IntoResponse {
    String::from("ok").into_response()
}

pub fn api_router() -> Router<AppState> {
    Router::new()
        .nest("/", route(&[("/", get(hello))]))
        .nest("/invoice_generator", route_invoice_generator())
        .nest("/job", route_job())
}

pub fn route(routes: &[(&str, MethodRouter<AppState>)]) -> Router<AppState> {
    let mut router = Router::<AppState>::new();
    for (path, route) in routes {
        router = router.route(*path, route.to_owned());
    }
    router
}
