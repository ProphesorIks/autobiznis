use crate::job::JobHandle;
use crate::models::JobModel;
use crate::routes::route;
use crate::schema::jobs;
use crate::utils::COUNTRY_NAMES;
use crate::{job::Job, AppState};
use axum::extract::{Multipart, State};
use axum::routing::post;
use axum::{routing::get, Json, Router};
use chrono::Datelike;
use csv::StringRecord;
use diesel::{Connection, RunQueryDsl};
use handlebars::Handlebars;
use serde::{Deserialize, Serialize};
use serde_json::{to_string, Value};
use serde_variant::to_variant_name;
use std::collections::{HashMap, HashSet};
use std::fmt::format;
use std::path::Path;
use std::process::Command;
use std::sync::atomic::AtomicI32;
use tokio::fs;
use strum::{EnumIter, IntoEnumIterator, Display};

#[derive(EnumIter, Debug, Display, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum Template {
    Eu,
    EuVat,
    Lt,
    NonEu,
    LtPvm,
    EuVatEur,
    EuEur,
    NonEuEur,
    EuGbp,
    EuGbpVat,
    NonEuGbp,
    LtEur,
    LtVatEur,
    LtGbp,
    LtGbpVat
}

use lazy_static::lazy_static;
lazy_static! {
    pub static ref HBS: Handlebars<'static> = {
        let mut h = Handlebars::new();

        for vname in vec![
            to_variant_name(&Template::Eu).expect("suk"),
            to_variant_name(&Template::EuVat).expect("suk"),
            to_variant_name(&Template::Lt).expect("suk"),
            to_variant_name(&Template::NonEu).expect("suk"),
            to_variant_name(&Template::LtPvm).expect("suk"),
            to_variant_name(&Template::EuVatEur).expect("suk"),
            to_variant_name(&Template::EuEur).expect("suk"),
            to_variant_name(&Template::NonEuEur).expect("suk"),
            to_variant_name(&Template::EuGbp).expect("suk"),
            to_variant_name(&Template::EuGbpVat).expect("suk"),
            to_variant_name(&Template::NonEuGbp).expect("suk"),
            to_variant_name(&Template::LtEur).expect("suk"),
            to_variant_name(&Template::LtVatEur).expect("suk"),
            to_variant_name(&Template::LtGbp).expect("suk"),
            to_variant_name(&Template::LtGbpVat).expect("suk"),
        ]
        .iter()
        {
            h.register_template_file(
                vname,
                format!("assets/invoice_generator/templates/{}.hbs", vname),
            )
            .expect("failed to register hbs template");
        }

        h
    };

    pub static ref TEMPLATE_VARIANTS: String = {
        let mut r = String::new();
        for v in Template::iter() {
            r.push_str(&format!("{},", to_variant_name(&v).expect("enum var?")))
        }
        r
    };
}

fn date_to_lt(date: chrono::NaiveDate) -> String {
    // Define Lithuanian month names
    let months = [
        "sausio",
        "vasario",
        "kovo",
        "balandžio",
        "gegužės",
        "birželio",
        "liepos",
        "rugpjūčio",
        "rugsėjo",
        "spalio",
        "lapkričio",
        "gruodžio",
    ];

    // Get day, month, and year
    let day = date.day();
    let month = date.month() as usize;
    let year = date.year();

    // Format the date in Lithuanian long date style
    format!("{}m. {} {} d.", year, months[month - 1], day)
}

#[derive(Serialize)]
pub struct TplData {
    pub name: String,
    pub age: i32,
}

#[derive(Debug)]
pub enum CsvReadError {
    Date,
    EmptyField(String),
    FieldType(String),
}

#[derive(Debug, PartialEq)]
pub struct InvoiceRow {
    pub date: chrono::NaiveDate,
    pub customer: String,
    city: String,
    region: String,
    zip_code: String,
    country_code: String,
    sum_dollars: f64,
    exchange_rate: Option<f64>,
    sum_euros: f64,
    vat: Option<f64>,
    vat_number: Option<String>,
    pub template: Template,
}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct InvoiceTemplate {
    pub serial_num: String,
    date: String,
    buyer_address: String,
    buyer_name: String,
    total: String,
    total_usd: String,
    usd_rate: String,
    title: String,
    vat: String,
    vat_payer_number: String,
    total_before_vat: String,
    vat_total: String,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InvoiceMetadata {
    pub serial_start: u32,
}

pub fn template_from_invoice(row: InvoiceRow, serial: i32) -> InvoiceTemplate {
    let country_name = COUNTRY_NAMES.get(row.country_code.as_str()).unwrap();
    let address = format!(
        "{}, {}, {}, {} {}",
        country_name, row.city, row.region, row.country_code, row.zip_code
    );
    let title = if row.sum_euros >= 0.0 {
        "PVM SĄSKAITA FAKTURA"
    } else {
        "PVM KREDITINĖ SĄSKAITA FAKTURA"
    }
    .to_string();

    let vvat = row.vat.unwrap_or(100.0);
    let k = 1.0 + (0.01 * vvat);
    let vat_total = (row.sum_euros * (vvat / (vvat + 100.0)))
        .to_string()
        .replace(".", ",");
    let total_before_vat = (row.sum_euros * (1.0 / k)).to_string().replace(".", ",");

    InvoiceTemplate {
        serial_num: format!("SFATBEB {}", (10_000_000 + serial).to_string()),
        date: date_to_lt(row.date),
        buyer_address: address,
        buyer_name: row.customer,
        total: row.sum_euros.to_string().replace(".", ","),
        total_usd: row.sum_dollars.to_string().replace(".", ","),
        usd_rate: row
            .exchange_rate
            .map(|v| v.to_string())
            .unwrap_or("--".to_string()),
        title,
        vat: row.vat.map(|v| v.to_string()).unwrap_or("--".to_string()),
        vat_payer_number: row.vat_number.unwrap_or("--".to_string()),
        vat_total,
        total_before_vat,
    }
}

impl TryFrom<&csv::StringRecord> for InvoiceRow {
    type Error = CsvReadError;
    fn try_from(record: &csv::StringRecord) -> Result<Self, Self::Error> {
        fn get_optional_string(r: &StringRecord, idx: usize) -> Option<String> {
            r.get(idx).filter(|s| !s.is_empty()).map(String::from)
        }

        fn get_string(
            r: &StringRecord,
            idx: usize,
            empty_err: &str,
        ) -> Result<String, CsvReadError> {
            get_optional_string(r, idx).ok_or(CsvReadError::EmptyField(format!("at: {}, error: {}", idx, empty_err.to_string())))
        }

        fn get_f64(str: &str, parser_err: &str) -> Result<f64, CsvReadError> {
            str.trim()
                .replace(",", ".")
                .parse::<f64>()
                .map_err(|_| CsvReadError::FieldType(parser_err.to_string()))
        }

        let date = get_string(&record, 0, "date")?;
        let date = chrono::NaiveDate::parse_from_str(&date, "%b %d, %Y")
            .map_err(|_| CsvReadError::Date)?;

        let customer = get_string(&record, 1, "customer")?;
        let city = get_string(&record, 2, "city")?;
        let region = get_string(&record, 3, "region")?;
        let zip_code = get_string(&record, 4, "zip_code")?;
        let country_code = get_string(&record, 5, "country_code")?;
        let sum_dollars = get_string(&record, 6, "sum_dollars")?;
        let sum_dollars = get_f64(&sum_dollars, "sum_dollars(f64)")?;
        let exchange_rate = get_optional_string(&record, 7);
        let exchange_rate = exchange_rate
            .map(|er| match er.as_ref() {
                "EUR" => None,
                _ => get_f64(&er, "exchange_rate(64)").ok(),
            })
            .flatten();

        let sum_euros = get_string(&record, 8, "sum_euros")?;
        let sum_euros = get_f64(&sum_euros, "sum_euros(f64)")?;
        let vat = get_optional_string(&record, 9);
        let vat = vat.map(|v| get_f64(&v, "vat(f64)")).transpose()?;
        let vat_number = get_optional_string(&record, 10);
        let template = get_optional_string(&record, 11)
            .map(|v| {
                serde_json::from_str::<Template>(&format!("\"{}\"", &v))
                    .map_err(|_| CsvReadError::FieldType(format!("template({})\n    Turi būti vienas iš: ({})", v, &*TEMPLATE_VARIANTS)))
            })
            .transpose()?;
        let template = match template {
            None => match country_code.to_uppercase().as_str() {
                "LT" => {
                    if let Some(_) = vat_number {
                        Template::LtPvm
                    } else {
                        Template::Lt
                    }
                }
                _ => match is_cc_eu(&country_code) {
                    true => {
                        if let Some(_) = exchange_rate {
                            if let Some(_) = vat_number {
                                Template::EuVatEur
                            } else {
                                Template::EuEur
                            }
                        } else {
                            if let Some(_) = vat_number {
                                Template::EuVat
                            } else {
                                Template::Eu
                            }
                        }
                    }
                    false => {
                        if let Some(_) = exchange_rate {
                            Template::NonEu
                        } else {
                            Template::NonEuEur
                        }
                    }
                },
            },
            Some(t) => t,
        };

        Ok(InvoiceRow {
            date,
            customer,
            city,
            region,
            zip_code,
            country_code,
            sum_dollars,
            exchange_rate,
            sum_euros,
            vat,
            vat_number,
            template,
        })
        // Err(CsvReadError::General)
    }
}

fn is_cc_eu(country_code: &str) -> bool {
    let eu_countries: HashSet<&str> = vec![
        "AT", "BE", "BG", "HR", "CY", "CZ", "DK", "EE", "FI", "FR", "DE", "GR", "HU", "IE", "IT",
        "LV", "LT", "LU", "MT", "NL", "PL", "PT", "RO", "SK", "SI", "ES", "SE",
    ]
    .into_iter()
    .collect();
    let upcs = country_code.to_uppercase();
    eu_countries.contains(upcs.as_str())
}

pub fn route_invoice_generator() -> Router<AppState> {
    async fn get_version() -> Json<JobHandle> {
        let a = InvoiceRow {
            date: chrono::NaiveDate::from_ymd_opt(2024, 3, 29).unwrap(),
            customer: "Stephan Moderer".to_string(),
            city: "Augsburg".to_string(),
            region: "--".to_string(),
            zip_code: "86152".to_string(),
            country_code: "DE".to_string(),
            sum_dollars: -665.0,
            exchange_rate: Some(0.901),
            sum_euros: -599.325,
            vat: None,
            vat_number: None,
            template: (Template::Eu),
        };

        Json(Job::default().into())
    }

    async fn start(mut multipart: Multipart) -> Json<JobHandle> {
        let mut file: Option<bytes::Bytes> = None;
        let mut data: HashMap<String, String> = HashMap::new();
        while let Some(mut f) = multipart.next_field().await.unwrap() {
            let name = f.name().unwrap();
            match name {
                "file" => {
                    file = Some(f.bytes().await.unwrap());
                }
                name => {
                    data.insert(name.to_string(), f.text().await.unwrap());
                }
            }
        }

        let mdata = InvoiceMetadata {
            serial_start: data
                .get("serialStart")
                .expect("serialStart not set")
                .parse()
                .expect("serial start is NaN"),
        };

        let job = Job::start(Some(
            serde_json::to_value(&mdata).expect("Couldn't convert to value ://"),
        ))
        .unwrap();
        let ppp = job.get_path_out();

        let fff = file.unwrap();
        let jjj: JobModel = (&job).into();
        let r = crate::database::DATABASE
            .get()
            .unwrap()
            .transaction(|conn| {
                let r = diesel::insert_into(jobs::table).values(&jjj).execute(conn);
                r
            });
        println!("cj: {}", r.unwrap());

        let mut p_pp = job.get_path_in().clone(); //.push("in.csv");
        p_pp.push("in.csv");
        fs::write(p_pp, fff).await.expect("couldn't write to file");
        Json(job.into())
    }

    route(&[("/version", get(get_version)), ("/start", post(start))])
}

#[cfg(test)]
mod test {
    use chrono::NaiveDate;
    use csv::ReaderBuilder;

    use crate::routes::invoice_generator::Template;

    use super::InvoiceRow;
    #[test]
    fn csv_parsing() {
        let data = r#"
Data,Vardas Pavardė,Miestas,"Regionas, provincija",Pašto kodas,Šalis,Suma doleriais,Valiutos kursas,Suma eurais,pvm,Vat mokėtojo kodas,Template
"Mar 29, 2024",Stephan Moderer,Augsburg,--,86152,DE,"-665,0","0,901","-599,325",,,eu
"Mar 27, 2024",Paljko Sjar,Tržaška cesta 2 - recepcija,Ljubljana,1000,SI,"60","1,136","69,053","21,4",abcd,eu_gbp"#;
        let mut rcs = ReaderBuilder::new()
            .delimiter(b',')
            .from_reader(data.as_bytes())
            .into_records();
        let r1 = rcs.next().unwrap();
        let r1 = r1.unwrap();
        let r1 = InvoiceRow::try_from(&r1).unwrap();

        assert_eq!(
            r1,
            InvoiceRow {
                date: NaiveDate::from_ymd_opt(2024, 3, 29).unwrap(),
                customer: "Stephan Moderer".to_string(),
                city: "Augsburg".to_string(),
                region: "--".to_string(),
                zip_code: "86152".to_string(),
                country_code: "DE".to_string(),
                sum_dollars: -665.0,
                exchange_rate: Some(0.901),
                sum_euros: -599.325,
                vat: None,
                vat_number: None,
                template: (Template::Eu),
            }
        );

        let r2 = rcs.next().unwrap();
        let r2 = r2.unwrap();
        let r2 = InvoiceRow::try_from(&r2).unwrap();

        assert_eq!(
            r2,
            InvoiceRow {
                date: NaiveDate::from_ymd_opt(2024, 3, 27).unwrap(),
                customer: "Paljko Sjar".to_string(),
                city: "Tržaška cesta 2 - recepcija".to_string(),
                region: "Ljubljana".to_string(),
                zip_code: "1000".to_string(),
                country_code: "SI".to_string(),
                sum_dollars: 60.0,
                exchange_rate: Some(1.136),
                sum_euros: 69.053,
                vat: Some(21.4),
                vat_number: Some("abcd".to_string()),
                template: (Template::EuGbp),
            }
        )
    }
}
