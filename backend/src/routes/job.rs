use std::{fmt::format, fs::File, path::PathBuf};

use axum::{
    extract::{self, State},
    http::{HeaderMap, HeaderValue, Response, StatusCode},
    response::IntoResponse,
    routing::{get, post},
    Json, Router,
};
use diesel::{Connection, QueryDsl, QuerySource, RunQueryDsl};
use serde::Deserialize;
use tokio::io::{self, AsyncReadExt};

use crate::{
    database::DATABASE,
    job::{Job, JobHandle, JobStatus, JobType},
    models::JobModel,
    routes::route,
    schema::{self, jobs},
    AppState,
};

pub fn route_job() -> Router<AppState> {
    async fn get_job_status(
        extract::Path(sudi): extract::Path<uuid::Uuid>,
        State(state): State<AppState>,
    ) -> impl IntoResponse {
        let bubu = sudi.to_string();

        let rs: Result<JobModel, _> = crate::database::DATABASE
            .get()
            .unwrap()
            .transaction(|conn| jobs::table.find(&bubu).first(conn));
        match rs {
            Ok(r) => Json(Job::try_from(&r).unwrap()).into_response(),
            Err(_) => (StatusCode::NOT_FOUND, "").into_response(),
        }
    }

    async fn create_job(State(state): State<AppState>) -> Json<JobHandle> {
        let job = Job::start(None).unwrap();
        let jjj: JobModel = (&job).into();
        let r = crate::database::DATABASE
            .get()
            .unwrap()
            .transaction(|conn| {
                let r = diesel::insert_into(jobs::table).values(&jjj).execute(conn);
                r
            });
        println!("cj: {}", r.unwrap());
        Json(job.into())
    }

    async fn get_artifact(
        extract::Path(sudi): extract::Path<uuid::Uuid>,
    ) -> Result<axum::response::Response, StatusCode> {
        let bubu = sudi.to_string();
        let rs: JobModel = crate::database::DATABASE
            .get()
            .unwrap()
            .transaction(|conn| jobs::table.find(&bubu).first(conn))
            .map_err(|_| StatusCode::NOT_FOUND)?;

        let rs: Job = (&rs).try_into().expect("suka");

        match rs.job_status {
            JobStatus::Finished => {}
            JobStatus::Failed => return Err(StatusCode::INTERNAL_SERVER_ERROR),
            _ => return Err(StatusCode::ACCEPTED),
        }

        let mut pp = rs.get_path_out();
        pp.push("out.zip");

        // Attempt to open the file
        let mut file: tokio::fs::File = tokio::fs::File::open(pp)
            .await
            .map_err(|_| StatusCode::NOT_FOUND)?;

        // Read the contents of the file
        let mut contents = Vec::new();
        file.read_to_end(&mut contents)
            .await
            .map_err(|_| StatusCode::INTERNAL_SERVER_ERROR)?;

        // Create response headers
        let mut headers = HeaderMap::new();
        headers.insert("Content-Type", HeaderValue::from_static("application/zip"));
        headers.insert(
            "Content-Disposition",
            HeaderValue::from_static("attachment; filename=\"invoices.zip\""),
        );
        // Create the response with the file contents
        Ok((StatusCode::OK, headers, contents).into_response())
    }

    route(&[
        ("/status/:id", get(get_job_status)),
        ("/artifact/:id", get(get_artifact)),
        ("/", post(create_job)),
    ])
}
