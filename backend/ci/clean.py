import shutil, os


def main():
    shutil.rmtree("artifacts")
    os.mkdir("artifacts")

if __name__ == "__main__":
    main()
